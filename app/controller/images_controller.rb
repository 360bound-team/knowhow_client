class ImagesController < ApplicationController
	require 'open-uri'

    class NotFoundError < StandardError; end

    rescue_from NotFoundError do |exception|
      render json: {message: "Content Not Found - 404", error_code: 'ERROR_CONTENT_NOT_FOUND'}, status: 404
    end


	def show
		begin
			rempte_conf = KnowhowClient::config
			remote_object = KnowhowClient::remote_object(:images, params[:id], request.host_with_port, setting_by_name('api_token'))
			if remote_object.present?
				if params[:size].present? && params[:size] != 'original'
					data = open("#{rempte_conf.server[:url]}/#{remote_object['file'][params[:size]]['url']}")
				else
					data = open("#{rempte_conf.server[:url]}/#{remote_object['file']['url']}")
				end
				send_data IO.binread(data), :filename => remote_object['name'], type: 'image/jpeg', disposition: params[:disposition] ||= 'inline'
			end
		rescue Exception => e
            Rails.logger.warn "KnowhowClient remote_object error: #{e}" 
            raise NotFoundError.new	
		end
	end
end