require "knowhow_client/version"
require "config/initializers/conf"
require "knowhow_client/api"

module KnowhowClient
    class Engine < ::Rails::Engine
        engine_name 'knowhow_client'
    end 

    def self.config
        @config ||= Config.new
    end

    def self.configure
        yield config
    end

    def self.remote_object_list(params={})
        Api.get_remote_object_list(params)
    end

    def self.remote_object(type, id, hostname, api_token)
        Api.get_remote_object(type, id, hostname, api_token)
    end
end
