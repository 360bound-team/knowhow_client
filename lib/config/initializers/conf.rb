module KnowhowClient
    class Config    
        attr_accessor :server, :locale, :remote_func

        COULD_NOT_CONNECT = "COULD_NOT_CONNECT_TO_SERVER"

        def initialize
            @server = { url: 'http://www.knowhowmd.com' } 
            @locale = 'en'

            @remote_func = {
                disorders:   "/api/v1/#{@locale}/disorders",
                riskfactors: "/api/v1/#{@locale}/riskfactors",
                symptoms:    "/api/v1/#{@locale}/symptoms",
                diagnostics: "/api/v1/#{@locale}/diagnostics",
                supports:    "/api/v1/#{@locale}/supports",
                articles:    "/api/v1/#{@locale}/articles", 
                treatments:  "/api/v1/#{@locale}/treatments", 
                anatomies:   "/api/v1/#{@locale}/anatomies", 
                images:      "/api/v1/#{@locale}/images",
                entries:     "/api/v1/#{@locale}/entries",
                doctors:     "/api/v1/#{@locale}/doctors",
                search:     "/api/v1/#{@locale}/search",
            }
        end
    end
end
