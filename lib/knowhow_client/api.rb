module KnowhowClient
    module Api
        require 'rest_client'
        class ContentError < StandardError; end   
        class CouldNotConnectError < StandardError; end   
        class FunctionNotFoundError < StandardError; end   
          
        def self.get_remote_object_list(params={})
            # Check bad use
            params[:type] = params['type'].to_sym if params['type'].present?
            params[:sub_type] = params['sub_type'].to_sym if params['sub_type'].present?
            params[:hostname] = params['hostname'].to_sym if params['hostname'].present?
            params[:api_token] = params['api_token'].to_sym if params['api_token'].present?

            raise ContentError.new, "Parameter :type not found" if params.member?(:type).blank?
            raise ContentError.new, "Parameter :hostname not found" if params.member?(:hostname).blank?
            raise ContentError.new, "Parameter :api_token not found" if params.member?(:api_token).blank?
      
            api_url = KnowhowClient.config.remote_func[params[:type]]
            raise FunctionNotFoundError.new if !api_url.present?

            begin     
                params_hash = params.map{|k,v| {k => v}}.reduce(:merge)                
                params_hash[:type] = params_hash[:sub_type] 
                params_hash.except!(:type) if params.member?(:sub_type).blank?
                params_hash.except!(:sub_type)

                response = RestClient.get "#{KnowhowClient.config.server[:url]}#{api_url}", { params: params_hash }

                if response && response.code == 200
                    JSON.parse(response.body)
                else
                    Rails.logger.warn "remote_object_list error: #{response.code}" if response
                end
            rescue Exception => e
                Rails.logger.warn "KnowhowClient remote_object_list error: #{e}" 
                raise ContentError.new, e
            end        
        end  

        def self.get_remote_object(type, id, hostname, api_token)
            api_url = KnowhowClient.config.remote_func[type.to_sym]
            raise FunctionNotFoundError.new if !api_url.present?

            begin
                response = RestClient.get "#{KnowhowClient.config.server[:url]}#{api_url}/#{id}", { params: { hostname: hostname, api_token: api_token } }
                if response && response.code == 200
                    JSON.parse(response.body)
                else
                    Rails.logger.warn "remote_object error: #{response.code}" if response
                end
            rescue Exception => e
                Rails.logger.warn "KnowhowClient remote_object error: #{e}" 
                raise ContentError.new, e
            end        
        end 
    end
end
