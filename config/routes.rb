KnowhowClient::Engine.routes.draw do
	get 'images/:id(/:size)' => 'images#show', as: :image, defaults: { size: 'large' }
end